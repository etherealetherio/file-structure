<?php

/**
 * @package ethereal-etherio
 * @license MIT
 * @version v1.0.0
 * @link    http://bitbucket.org/etherealetherio/file-structure
 */
require "../vendor/autoload.php";

define('REQUEST_START', microtime(true));

$app = require('../build/app.php');

$app->make(App\Http\Kernel::class);

$app
    ->create(new Request, new Response)
    ->response()
    ->terminate();

exit(0);
